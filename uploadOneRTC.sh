#/bin/bash

if [ $# -ne 4 ]; then
    echo "usage: $0 artifactId version repository aarFile"
    exit -1
fi

artifactId=$1
version=$2
repository=$3
aarFile=$4

echo "mvn deploy:deploy-file -Durl=http://nexus.arashivision.com:9999/repository/maven-${repository}/ -Dfile="$aarFile" -Dpackaging=aar -DrepositoryId=arashivision -DgroupId=com.arashivision.minicamera -DartifactId=${artifactId} -Dversion=${version}"


mvn deploy:deploy-file -Durl=http://nexus.arashivision.com:9999/repository/maven-${repository}/ -Dfile="$aarFile" -Dpackaging=aar -DrepositoryId=arashivision -DgroupId=com.arashivision.minicamera -DartifactId=${artifactId} -Dversion=${version} || exit $?

echo >> onertc_versions.txt
date >> onertc_versions.txt
echo artifactId: $artifactId version=$version repository=$repository aarFile=$aarFile >> onertc_versions.txt
