package com.arashivision.webrtc;

import org.webrtc.PeerConnection;

import java.util.List;

/**
 * Created by vans on 27/12/17.
 */

public class RTCConnectInfo {
    //eg:https://vcall.insta360.cn/webrtc
    public String url;//host + nsp
    //not used yet
    public String host;
    public String nsp;
    //room id
    public String roomId;
    //normally uuid
    public String userId;
    //OneRTCConstants.ROLE
    public String role;
    //generate randomly before connect every time
    public String sessionId;
    //info about our camera(sn + one ...)
    public String userAgent;
    //get from our own server
    public List<PeerConnection.IceServer> turnServers;
}
