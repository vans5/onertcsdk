package com.arashivision.webrtc;

/**
 * Created by vans on 30/12/17.
 */

public class OneRTCConstants {
    public static class ROLE
    {
        public static String CALLER = "caller";
        public static String CALLEE = "callee";
    }

    public static class ERROR
    {
        public static String CHANEL_CLOSE = "channel_close";
        public static String SOCKET_FORMAT_ERROR = "socket_format_error";
        public static String ICE_DISCONNECT = "ice_disconnect";
        //room that not exists
        public static String ROOM_DENY = "room_deny";
    }
}
