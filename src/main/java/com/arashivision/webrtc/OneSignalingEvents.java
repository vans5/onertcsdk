package com.arashivision.webrtc;

import org.webrtc.IceCandidate;
import org.webrtc.SessionDescription;

/**
 * Created by vans on 30/12/17.
 */

public interface OneSignalingEvents {

    void onConnectedSignalServer(final ConnectSignalServerInfo params);

    /**
     * Callback fired once remote SDP is received.
     */
    void onRemoteDescription(final SessionDescription sdp);

    /**
     * Callback fired once remote Ice candidate is received.
     */
    void onRemoteIceCandidate(final IceCandidate candidate);

    /**
     * Callback fired once remote Ice candidate removals are received.
     */
    void onRemoteIceCandidatesRemoved(final IceCandidate[] candidates);

    /**
     * Callback fired once channel is closed.
     */
    void onChannelClose();

    /**
     * Callback fired once channel error happened.
     */
    void onChannelError(final String description);
}
