package com.arashivision.webrtc;

/**
 * Created by vans on 26/12/17.
 */

public class InitRequest {
    public String user_id;
    public String room_id;
    public String role;

    public InitRequest(String user_id,String room_id,String role)
    {
        this.user_id = user_id;
        this.room_id = room_id;
        this.role = role;
    }
}
