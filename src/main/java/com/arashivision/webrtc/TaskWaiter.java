package com.arashivision.webrtc;

import java.util.concurrent.CountDownLatch;

public class TaskWaiter {
        public TaskWaiter() {
            mLatch = new CountDownLatch(1);
        }
        public void await() {
            try {
                mLatch.await();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        public void done() {
            mLatch.countDown();
        }
        private CountDownLatch mLatch;
    }