package com.arashivision.webrtc;

import org.webrtc.PeerConnection;

/**
 * Created by vans on 28/12/17.
 */

public class ConnectSignalServerInfo {
    public SocketActionInfo mSoketActionInfo;
    public AppRTCClient.SignalingParameters signalingParameters;
}
