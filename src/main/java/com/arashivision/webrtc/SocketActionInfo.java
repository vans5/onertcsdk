package com.arashivision.webrtc;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;


/**
 * Created by vans on 27/12/17.
 */

public class SocketActionInfo {
    public static final String TAG = "SocketActionInfo";
    public Data mData;
    public Meta mMeta;

    public static final String PAYLOAD_CANDIDATE = "candidate";
    public static final String PAYLOAD_SDP = "sdp";
    public static final String PAYLOAD_DELETE = "deleted";
    public static final String PAYLOAD_HANGUP = "hangup";
    public static final String PAYLOAD_JOIN = "join";
    public static final String PAYLOAD_RECONNECT = "reconnect";

    public static final String SDP_OFFER = "offer";
    public static final String SDP_ANSWER = "answer";

    private static final String ACTION_DENY = "deny";
    private static final String ACTION_EXCHANGE = "exchange";
    private static final String ACTION_BROADCAST = "broadcast";


    //    private static final String RECONNECT = "reconnect";

    public static class Meta
    {
        public long timestamp;
        public String client;
        public String target;
    }

    public static class Data
    {
        public String action;
        public PayLoad mPayLoad;
        public static class PayLoad
        {
            public String type;
            public String role;
            public String message;
            public SDPData mSDPData;
            public IceCandidate mCandidateData;
            public static class SDPData
            {
                public String type;//offser or answer
                public String sdp;
            }
        }
    }

    public IceCandidate getCandidate()
    {
        return mData.mPayLoad.mCandidateData;
    }

    public static SocketActionInfo fromJson(JSONObject json)
    {
        SocketActionInfo mSocketActionInfo = new SocketActionInfo();
        try {
            //meta
            JSONObject jsonMeta = json.getJSONObject("meta");
            mSocketActionInfo.mMeta = new SocketActionInfo.Meta();
            mSocketActionInfo.mMeta.timestamp = jsonMeta.getLong("timestamp");
            mSocketActionInfo.mMeta.client = jsonMeta.optString("client");
            mSocketActionInfo.mMeta.target = jsonMeta.optString("target");
            //data
            JSONObject jsonData = json.getJSONObject("data");
            mSocketActionInfo.mData = new SocketActionInfo.Data();
            mSocketActionInfo.mData.action =  jsonData.optString("action");
            if(mSocketActionInfo.mData.action != null)
            {
                Log.d(TAG," action " + mSocketActionInfo.mData.action);
                mSocketActionInfo.mData.mPayLoad = new SocketActionInfo.Data.PayLoad();
                //payload
                JSONObject jsonPayload = jsonData.getJSONObject("payload");
                mSocketActionInfo.mData.mPayLoad.type = jsonPayload.getString("type");
                if(mSocketActionInfo.mData.action.equals(ACTION_DENY))
                {
                    mSocketActionInfo.mData.mPayLoad.message = jsonPayload.getString("message");
                    Log.w(TAG,"action deny "
                            + mSocketActionInfo.mData.mPayLoad.type + " message "
                            + mSocketActionInfo.mData.mPayLoad.message);
                }
                else
                {
                    //payload data
                    if(mSocketActionInfo.mData.mPayLoad.type.equals(PAYLOAD_SDP))
                    {
                        mSocketActionInfo.mData.mPayLoad.role = jsonPayload.getString("role");

                        JSONObject jsonPayloadData = jsonPayload.getJSONObject("data");
                        mSocketActionInfo.mData.mPayLoad.mSDPData =  new SocketActionInfo.Data.PayLoad.SDPData();
                        mSocketActionInfo.mData.mPayLoad.mSDPData.type =jsonPayloadData.getString("type");
                        mSocketActionInfo.mData.mPayLoad.mSDPData.sdp =jsonPayloadData.getString("sdp");
                    }
                    else if(mSocketActionInfo.mData.mPayLoad.type.equals(PAYLOAD_CANDIDATE))
                    {
                        JSONObject jsonPayloadData = jsonPayload.getJSONObject("data");
                        mSocketActionInfo.mData.mPayLoad.mCandidateData =
                                new IceCandidate(jsonPayloadData.getString("sdpMid"),
                                        jsonPayloadData.getInt("sdpMLineIndex"),jsonPayloadData.getString("candidate"));
                    }
                    else if(mSocketActionInfo.mData.mPayLoad.type.equals(PAYLOAD_HANGUP))
                    {
                        mSocketActionInfo.mData.mPayLoad.role = jsonPayload.getString("role");
                        JSONObject jsonPayloadData = jsonPayload.getJSONObject("data");
                        mSocketActionInfo.mData.mPayLoad.message = jsonPayloadData.getString("message");
                        Log.w(TAG,"other type "
                                + mSocketActionInfo.mData.mPayLoad.type + " message "
                                + mSocketActionInfo.mData.mPayLoad.message);
                    }
                    else
                    {
                        Log.w(TAG,"other payload type "
                                + mSocketActionInfo.mData.mPayLoad.type);
                    }
                }
            }
            else
            {
                Log.d(TAG," action null ");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            mSocketActionInfo = null;
        }
        return mSocketActionInfo;
    }

    public boolean equalType(String type)
    {
        if(mData.mPayLoad.type.equals(type))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String getClient()
    {
        return mMeta.client;
    }

    public boolean equalClient(String client)
    {
        if(mMeta.client.equals(client))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String getRole()
    {
        return mData.mPayLoad.role;
    }

    public boolean equalRole(String role)
    {
        if(mData.mPayLoad.role.equals(role))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String getTarget()
    {
        return mMeta.target;
    }

    public String getPayloadType()
    {
        return mData.mPayLoad.type;
    }

    public String getSDPType()
    {
        return mData.mPayLoad.mSDPData.type;
    }

    public String getSDP()
    {
        return mData.mPayLoad.mSDPData.sdp;
    }

    public boolean equalTarget(String target)
    {
        if(mMeta.target.equals(target))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isActionDeny()
    {
        if(mData.action.equals(ACTION_DENY))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
