package com.arashivision.webrtc;

/**
 * Created by vans on 26/12/17.
 */

public class TurnServerInfo {
    public String url;
    public String username;
    public String credential;

    public TurnServerInfo(String url, String username,String credential)
    {
        this.url = url;
        this.username = username;
        this.credential = credential;
    }
}
