package com.arashivision.webrtc;

import java.sql.ClientInfoStatus;

import io.crossbar.autobahn.wamp.Client;

/**
 * Created by vans on 27/12/17.
 */

public class OnLineInfo {

    private static final String PARICIPATOR = "participator";
    public String target;
    public String action;
    public String message;

    public Client mClient[];

    private static class ClientInfo
    {
        public String transport;
        public String userAgent;
        public String userId;
        public String sessionId;
        public String roomId;
        public String role;
    }
}
