package com.arashivision.webrtc;

import com.arashivision.webrtc.PeerConnectionClient.DataChannelParameters;
/**
 * Created by vans on 30/12/17.
 */

public class RTCInitParam {
    // Video call enabled flag.
    public   boolean videoCallEnabled = true;
    public   boolean loopback = false;
    public   boolean tracing = false;
    public   int videoWidth = 0;
    public   int videoHeight = 0;
    public   int videoFps = 0;
    public   int videoMaxBitrate = 0;
    public   String videoCodec = "H264 Baseline";//VP8";
    public   boolean videoCodecHwAcceleration = true;
    public   boolean videoFlexfecEnabled = false;
    public   int audioStartBitrate = 0;
    public   String audioCodec = "OPUS";
    public   boolean noAudioProcessing = false;
    public   boolean aecDump= false;
    public   boolean useOpenSLES= false;
    public   boolean disableBuiltInAEC= false;
    public   boolean disableBuiltInAGC= false;
    public   boolean disableBuiltInNS= false;
    public   boolean enableLevelControl= false;
    public   boolean disableWebRtcAGCAndHPF= false;
    public DataChannelParameters dataChannelParameters;


    public RTCInitParam()
    {
        dataChannelParameters = new DataChannelParameters(true,-1,-1,"",false,-1);
    }
}
