package com.arashivision.webrtc;

/**
 * Created by vans on 30/12/17.
 */

public interface RTCInfoListener {
    void onLocalFPS(int fps);

    void onRemoteFPS(int fps);
    void onRemoteBitrate(int bitrate);
    void onRemoteReconnect();
}
